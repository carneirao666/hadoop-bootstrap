 ###############################################################################################
#                                                                                               #
# ramtricks/hadoop-bootstrap - Another single node Hadoop environment based on Ubuntu Docker    #
#                               container.                                                      #
#                                                                                               #
# Version: 0.0.20-r1                                                                            #
#                                                                                               #
#                                                                                               #
# Author: André Carneiro                                                                        #
#                                                                                               #
 ###############################################################################################
FROM ubuntu:18.04
USER root
WORKDIR /root/Install

# set environment vars
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64

ENV HADOOP_BASE /opt/hadoop
ENV HADOOP_HOME /opt/hadoop/current
ENV HADOOP_VERSION=2.7.7
ENV HADOOP_CONF_DIR /opt/hadoop/current/etc/hadoop
ENV HADOOP_MAPRED_HOME=$HADOOP_HOME
ENV YARN_HOME=$HADOOP_HOME

ENV HIVE_BASE /opt/hive
ENV HIVE_HOME /opt/hive/current
ENV HIVE_VERSION=2.3.7

ENV SPARK_BASE /opt/spark
ENV SPARK_HOME /opt/spark/current
ENV SPARK_VERSION=2.4.7
ENV SPARK_MAJOR_VERSION=3

ENV HDFS_NAMENODE_USER root
ENV HDFS_DATANODE_USER root
ENV HDFS_SECONDARYNAMENODE_USER root
ENV YARN_RESOURCEMANAGER_USER root
ENV YARN_NODEMANAGER_USER root

# configuring tz to avoid problems with interaction problems with tzdata package
ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install packages
RUN \
  apt-get update && apt-get install -y \
  net-tools \
  sudo \
  curl \
  ssh \
  rsync \
  vim \
  openjdk-8-jdk \
  maven \
  mysql-server \
  libmysql-java \
  xz-utils \
  build-essential \
  checkinstall \
  libreadline-gplv2-dev \
  libncursesw5-dev libssl-dev \
  libsqlite3-dev \
  tk-dev \
  libgdbm-dev \
  libc6-dev \
  libbz2-dev \
  libffi-dev \
  zlib1g-dev \
  git

 ########################################
#                                        #
#     Download and configuring Hadoop    #
#                                        #
 ########################################

# 'https://www-us.apache.org/dist/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz'
# download and extract hadoop, set JAVA_HOME in hadoop-env.sh, update path
#RUN curl --progress-bar https://downloads.apache.org/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz \
#-o "hadoop-$HADOOP_VERSION.tar.gz"

# FOR TESTING ONLY
COPY *.tar.gz /root/Install/
COPY *.tgz /root/Install/

RUN mkdir -p $HADOOP_BASE \
&& tar -xzvmf hadoop-$HADOOP_VERSION.tar.gz -C $HADOOP_BASE/ \
&& /bin/bash -c "cd /opt/hadoop;ln -rs hadoop-$HADOOP_VERSION/ current"

RUN echo "export JAVA_HOME=$JAVA_HOME" >> $HADOOP_HOME/etc/hadoop/hadoop-env.sh; \
echo "PATH=$PATH:$HADOOP_HOME/bin" >> ~/.bashrc

# create ssh keys
RUN  ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
RUN  cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
RUN  chmod 0600 ~/.ssh/authorized_keys

# copy hadoop configs
COPY conf/hadoop/*xml $HADOOP_HOME/etc/hadoop/

# create hduser user
RUN useradd -m -s /bin/bash -p $(perl -e 'print crypt($ARGV[0], "password")' 'hadoop') hduser \
&& groupadd hdfs \
&& groupadd supergroup \
&& groupadd hive \
&& usermod -aG hdfs hduser \
&& usermod -aG sudo hduser \
&& usermod -aG supergroup hduser \
&& mkdir ~hduser/.ssh

# create ssh keys
RUN  ssh-keygen -t rsa -P '' -f ~hduser/.ssh/id_rsa \
&& cat ~/.ssh/id_rsa.pub >> ~hduser/.ssh/authorized_keys \
&&  chmod 0600 ~hduser/.ssh/authorized_keys

# copy script to start hadoop
COPY start-hadoop.sh /root/start-hadoop.sh
COPY initial-setup.sh /root/initial-setup.sh

# Adding execute permission to scripts
RUN chmod +x /root/start-hadoop.sh /root/initial-setup.sh


 #########################################
#                                         #
#      Download and configuring Hive      #
#                                         #
 #########################################

# Downloading
#RUN curl --progress-bar https://downloads.apache.org/hive/hive-$HIVE_VERSION/apache-hive-$HIVE_VERSION-bin.tar.gz \
#-o "apache-hive-$HIVE_VERSION-bin.tar.gz"

# Decompressing and Creating $HIVE_HOME
RUN mkdir -p $HIVE_BASE \
&& tar -xzvmf apache-hive-$HIVE_VERSION-bin.tar.gz -C $HIVE_BASE \
&& /bin/bash -c "cd $HIVE_BASE;ln -s apache-hive-$HIVE_VERSION-bin/ current"

# Copying hive-site.xml file to $HIVE_HOME/conf
COPY conf/hive/hive-site.xml $HIVE_HOME/conf

# Link hive-site.xml on $HADOOP_CONF_DIR
RUN bash -c "cd $HADOOP_CONF_DIR/; ln -s $HIVE_HOME/conf/hive-site.xml ."

 #########################################
#                                         #
#    Configuring Hive Metastore + MySQL   #
#                                         #
#                                         #
 #########################################

RUN /etc/init.d/mysql start && mysqladmin -u root password hadoop \
&& mysql -e "CREATE USER 'hduser'@'127.0.0.1' IDENTIFIED BY 'hadoop'" \
&& mysql -e "GRANT ALL PRIVILEGES ON hive.* TO 'hduser'@'127.0.0.1';" \
&& mysql -e "CREATE DATABASE metastore_db" \
&& cd $HIVE_HOME/scripts/metastore/upgrade/mysql/ \
&& mysql -e "USE metastore_db;SOURCE $HIVE_HOME/scripts/metastore/upgrade/mysql/hive-schema-2.3.0.mysql.sql;" \
&& cd /root/Install

RUN /bin/bash -c "ln -s /usr/share/java/mysql-connector-java.jar $HIVE_HOME/lib/mysql-connector-java.jar"


 #########################################
#                                         #
#  Adujsting 'guava' lib on Hive          #
#                                         #
#                                         #
 #########################################

RUN rm -f $HIVE_HOME/lib/guava-19.0.jar
RUN ln -s $HADOOP_HOME/share/hadoop/common/lib/guava-27.0-jre.jar $HIVE_HOME/lib/guava-27.0-jre.jar

ENV PATH $PATH:$HIVE_HOME/bin


 #########################################
#                                         #
#      Download and configuring Spark     #
#                                         #
 #########################################

# Downloading Spark
#RUN wget https://downloads.apache.org/spark/spark-3.0.1/spark-3.0.1-bin-hadoop2.7.tgz

# Creating $SPARK_HOME on opt/spark
RUN mkdir -p $SPARK_BASE \
&& tar -xzvmf spark-$SPARK_VERSION-bin-hadoop2.7.tgz -C $SPARK_BASE/ \
&& /bin/bash -c "cd $SPARK_BASE;ln -s $SPARK_BASE/spark-$SPARK_VERSION-bin-hadoop2.7 $SPARK_BASE/current"

# Without this, Spark will never find warehouse dir
RUN ln -s $HIVE_HOME/conf/hive-site.xml $SPARK_HOME/conf/

ENV PATH $PATH:$SPARK_HOME/bin
ENV HADOOP_HOME /opt/hadoop/current
ENV HIVE_HOME /opt/hive/current
ENV SPARK_HOME /opt/spark/current
ENV TEZ_HOME /opt/tez/current
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
 #########################################
#                                         #
#     Exportando variáveis de ambiente    #
#     do Java, Hadoop, Hive e Spark       #
#                                         #
 #########################################

RUN echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" >> /etc/profile \
&& echo "export HADOOP_CONF_DIR=/opt/hadoop/current/etc/hadoop" >> /etc/profile \
&& echo "export HADOOP_VERSION=$HADOOP_VERSION" >> /etc/profile \
&& echo "export HADOOP_BASE=/opt/hadoop" >> /etc/profile \
&& echo "export HADOOP_HOME=/opt/hadoop/current" >> /etc/profile \
&& echo "export HIVE_HOME=/opt/hive/current" >> /etc/profile \
&& echo "export HIVE_VERSION=$HIVE_VERSION" >> /etc/profile \
&& echo "export HIVE_BASE=/opt/hive" >> /etc/profile \
&& echo "export SPARK_VERSION=$SPARK_VERSION" >> /etc/profile \
&& echo "export SPARK_BASE=/opt/spark" >> /etc/profile \
&& echo "export SPARK_MAJOR_VERSION=SPARK_MAJOR_VERSION" >> /etc/profile \
&& echo "export SPARK_HOME=/opt/spark/current" \
&& echo "export JAVA_HOME=$JAVA_HOME" >> $HADOOP_HOME/etc/hadoop/httpfs-env.sh \
&& echo "export JAVA_HOME=$JAVA_HOME" >> $HADOOP_HOME/etc/hadoop/mapred-env.sh \
&& echo "export JAVA_HOME=$JAVA_HOME" >> $HADOOP_HOME/etc/hadoop/yarn-env.sh \
&& echo "export JAVA_HOME=$JAVA_HOME" >> $HIVE_HOME/bin/hive-config.sh \
&& echo "export HDFS_NAMENODE_USER=root" >>/etc/profile \
&& echo "export HDFS_DATANODE_USER=root" >>/etc/profile \
&& echo "export HDFS_SECONDARYNAMENODE_USER=root" >>/etc/profile \
&& echo "export YARN_RESOURCEMANAGER_USER=root" >>/etc/profile \
&& echo "export YARN_NODEMANAGER_USER=root" >>/etc/profile \
&& echo "export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$HIVE_HOME/bin:$JAVA_HOME/bin:$SPARK_HOME/bin" >>/etc/profile \
&& echo "export CLASSPATH=$CLASSPATH:$HADOOP_HOME/lib/*:." >> /etc/profile \
&& echo "source /etc/profile" >> ~/.bashrc
#RUN echo "export JAVA_HOME=$JAVA_HOME" >> $SPARK_HOME/bin/load-spark-env.sh



 ########################################
#                                        #
#     Download and configuring Python    #
#     and Jupyter                        #
#                                        #
 ########################################

# Install pyspark

ENV PYTHON_VERSION 3.7.5

# For pandas
RUN apt install liblzma-dev

#RUN curl --progress-bar https://www.python.org/ftp/python/3.8.5/Python-3.8.5.tar.xz -o "Python-3.8.5.tar.xz"
RUN curl --progress-bar https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz -o "Python-$PYTHON_VERSION.tar.xz"
RUN rm /usr/bin/python3
RUN tar -xvf Python-$PYTHON_VERSION.tar.xz \
&& cd Python-$PYTHON_VERSION \
&& ./configure --enable-optimizations \
&& make altinstall \
&& ldconfig

# Linking python binaries
RUN rm /usr/bin/python3m
RUN ln -s /usr/local/bin/python3.7 /usr/bin/python3
RUN ln -s /usr/local/bin/python3.7m /usr/bin/python3m
RUN ln -s /usr/local/bin/python3.7 /usr/bin/python

USER hduser
WORKDIR /home/hduser

ENV PATH $PATH:/home/hduser/.local/bin:/usr/bin:/usr/local/bin:/bin:/sbin
RUN python3 -mpip install --upgrade pip --user
# The "saint triad" of Big data
RUN python3 -mpip install pyspark jupyter pandas --user

# Configuring ~hduser/.bashrc
RUN touch .bashrc
ENV PATH $PATH:$SPARK_HOME/bin
ENV HADOOP_HOME /opt/hadoop/current
ENV HIVE_HOME /opt/hive/current
ENV HADOOP_CLASSPATH=$HADOOP_HOME/lib/*
ENV SPARK_HOME /opt/spark/current
ENV SPARK_MAJOR_VERSION=2
ENV TEZ_HOME /opt/tez/current
ENV TEZ_JARS $TEZ_HOME/jars
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ENV HADOOP_CLASSPATH=$TEZ_HOME/conf:$TEZ_HOME/lib/*:$TEZ_JARS/*:$TEZ_JARS/lib/*:$HADOOP_HOME/lib/*
ENV CLASSPATH=$HADOOP_CLASSPATH:CLASSPATH

RUN echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" >> .bashrc \
&& echo "export CLASSPATH=/usr/share/java/*:$JAVA_HOME/lib/*" \
&& echo "export HADOOP_CONF_DIR=/opt/hadoop/current/etc/hadoop" >> .bashrc \
&& echo "export HADOOP_VERSION=$HADOOP_VERSION" >> .bashrc \
&& echo "export HADOOP_BASE=/opt/hadoop" >> .bashrc \
&& echo "export HADOOP_HOME=/opt/hadoop/current" >> .bashrc \
&& echo "export HIVE_HOME=/opt/hive/current" >> .bashrc \
&& echo "export HIVE_VERSION=$HIVE_VERSION" >> .bashrc \
&& echo "export HIVE_BASE=/opt/hive" >> .bashrc \
&& echo "export SPARK_VERSION=$SPARK_VERSION" >> .bashrc \
&& echo "export SPARK_BASE=/opt/spark" >> .bashrc \
&& echo "export SPARK_MAJOR_VERSION=$SPARK_MAJOR_VERSION" >> .bashrc \
&& echo "export SPARK_HOME=/opt/spark/current" \
&& echo "export HDFS_NAMENODE_USER=root" >>.bashrc \
&& echo "export HDFS_DATANODE_USER=root" >>.bashrc \
&& echo "export HDFS_SECONDARYNAMENODE_USER=root" >>.bashrc \
&& echo "export YARN_RESOURCEMANAGER_USER=root" >>.bashrc \
&& echo "export YARN_NODEMANAGER_USER=root" >>.bashrc \
&& echo "export TEZ_HOME=/opt/tez/current" >> .bashrc \
&& echo "export TEZ_CONF_DIR=$TEZ_HOME/conf" >> .bashrc \
&& echo "export TEZ_JARS=$TEZ_HOME/jars" >> .bashrc \
&& echo "export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$HIVE_HOME/bin:$JAVA_HOME/bin:$SPARK_HOME/bin" >> .bashrc \
&& echo "export PATH=$PATH:$HOME/.local/bin:$HADOOP_HOME/bin:$HIVE_HOME/bin:$SPARK_HOME/bin" >> .bashrc \
&& echo "export HADOOP_CLASSPATH=$TEZ_HOME/conf:$TEZ_HOME/lib/*:$TEZ_JARS/*:$TEZ_JARS/lib/*:$HADOOP_HOME/lib/*" >> .bashrc \
&& echo "export CLASSPATH=$HADOOP_CLASSPATH:CLASSPATH" >> .bashrc

USER root
WORKDIR /root/Install

 #######################################
#                                       #
#      Installing/Integrating Tez       #
#                                       #
 #######################################

# Install Proto Buffer 2.5.0
RUN tar -xzvmf protobuf-2.5.0.tar.gz
RUN cd protobuf-2.5.0 \
&& ./autogen.sh; ./configure; make; sudo make install; ldconfig

# Creating tez directories in /opt
ENV TEZ_HOME /opt/tez/current
RUN mkdir -p /opt/tez/tez-0.9.2/jars /opt/tez/tez-0.9.2/conf \
&& ln -s /opt/tez/tez-0.9.2 /opt/tez/current \
&& cd /root/Install/ \
&& tar -xzvmf apache-tez-0.9.2-dist.tar.gz \
&& cd /root/Install/apache-tez-0.9.2-dist/tez-dist/target \
&& tar -xzvmf tez-0.9.2-minimal.tar.gz -C $TEZ_HOME/jars/
COPY conf/tez/tez-site.xml /opt/tez/tez-0.9.2/conf/

## Finishing ##

# Cleanup
RUN apt purge -y maven git

# Given permission on /tmp/hive
RUN mkdir -p /tmp/hive \
&& chmod -R 777 /tmp/hive*

# expose various ports
EXPOSE 8088 8888 5000 50070 50075 50030 50060 9014 9090 9066
