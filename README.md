# ramtricks/hadoop-bootstrap

 A basic Hadoop single-node image for studying and PoCs.



## Version

0.0.21-r1

## Main Features

 * Hadoop 2.7.7 
 * Spark 2.4.7
 * Java 1.8.0_242
 * Hive 2.8 on Tez 0.9.2
 * Metastore on MySQL 5.7.31
 * Python 3.8.5
 * vim 8.0
 * Jupyter 4.4.0

## Installing

 `docker pull ramtricks/hadoop-bootstrap:0.0.20-r1`


## Setup a network

 `docker network create x`

## Starting 'core' services (Hadoop + Metastore on MySQL)

```bash

docker run --network x \
--name hadoop \
-uroot \
-w /root \
-d -it ramtricks/hadoop-bootstrap:0.0.20-r1 /bin/bash -c "./start-hadoop.sh"

```



## Using container to run ...



### Hive

`docker exec -uhduser -w/home/hduser -it hadoop bash`

then execute:

`hive`

### Spark

`docker exec -uhduser -w/home/hduser -it hadoop pyspark`

### Jupyter

1. Getting the container IP: 

```bash
 $ docker exec -uhduser -it hadoop ifconfig
   eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
           inet 172.24.0.2  netmask 255.255.0.0  broadcast 172.24.255.255
           ether 02:42:ac:18:00:02  txqueuelen 0  (Ethernet)
           RX packets 260  bytes 47382 (47.3 KB)
           RX errors 0  dropped 0  overruns 0  frame 0
           TX packets 198  bytes 1980574 (1.9 MB)
           TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

   lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
           inet 127.0.0.1  netmask 255.0.0.0
           loop  txqueuelen 1000  (Local Loopback)
           RX packets 5500  bytes 1886815 (1.8 MB)
           RX errors 0  dropped 0  overruns 0  frame 0
           TX packets 5500  bytes 1886815 (1.8 MB)
           TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```




2. Run `jupyter-notebook` command:

```bash
   
   $ docker exec -uhduser -it hadoop jupyter-notebook --ip 172.24.0.2 --port 9090
   [I 17:11:16.386 NotebookApp] Serving notebooks from local directory: /root
   [I 17:11:16.386 NotebookApp] Jupyter Notebook 6.1.4 is running at:
   [I 17:11:16.386 NotebookApp] http://172.24.0.2:9090/?token=71bfb56a829c22e0faacaa844f444a175a92bb473fa1bb67
   [I 17:11:16.386 NotebookApp]  or http://127.0.0.1:9090/?token=71bfb56a829c22e0faacaa844f444a175a92bb473fa1bb67
   [I 17:11:16.386 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
   [W 17:11:16.394 NotebookApp] No web browser found: could not locate runnable browser.
   [C 17:11:16.395 NotebookApp] 
       
       To access the notebook, open this file in a browser:
           file:///home/hduser/.local/share/jupyter/runtime/nbserver-2004-open.html
       Or copy and paste one of these URLs:
           http://172.24.0.2:9090/?token=71bfb56a829c22e0faacaa844f444a175a92bb473fa1bb67
        or http://127.0.0.1:9090/?token=71bfb56a829c22e0faacaa844f444a175a92bb473fa1bb67
   
   
```

   

Then, copy the IP address(not 127.0.0.1) with token and paste on your web browser!

That's it! You have now a mini Hadoop laboratory! Have fun!



## If you want to build from Dockerfile

 * Build using Dockerfile: 
   
   `docker build -t example_image:0.0.1 .`
 
 * Initialize HDFS and Metastore. For now, is not possible doing that from building process(working at it): 
   
   `docker run --name hadoop -uroot -w/root -it ramtricks/hadoop-bootstrap:0.0.18-r11 /bin/bash initial-setup.sh`
 
 * Commit the container(first, find the container ID with `docker ps -a` command). Then: 
   
   `docker commit <CONTAINER_ID> example_image:0.0.1`
 
 * Now you can proceed from "Start 'core' service" section
```
    docker run --network x \
    --name hadoop \
    -uroot \
    -w /root \
    -d -it example_image:0.0.1 /bin/bash -c "./start-hadoop.sh"
```
 * Finally, you can use the container services(hive, jupyter, pyspark, etc). Ex: `docker exec -uhduser -it hadoop hive`

