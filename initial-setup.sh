# Starting ssh server
/etc/init.d/ssh start

# Format namenode
echo "FORMATING NAMENODE"
yes | $HADOOP_HOME/bin/hdfs namenode -format

# Starting hadoop dfs and yarn services
echo "STARTING DFS"
yes | $HADOOP_HOME/sbin/start-dfs.sh
echo "STARTING YARN"
yes | $HADOOP_HOME/sbin/start-yarn.sh

# Preparing hdfs for hive
echo "PREPARING HDFS FOR HIVE"
$HADOOP_HOME/bin/hdfs dfs -mkdir -p /user/hive/warehouse
$HADOOP_HOME/bin/hdfs dfs -mkdir -p /tmp/hive
$HADOOP_HOME/bin/hdfs dfs -chmod 777 /tmp/
$HADOOP_HOME/bin/hdfs dfs -chmod 777 /user/hive/warehouse
$HADOOP_HOME/bin/hdfs dfs -chmod 777 /tmp/hive

# Preparing HDFS for hduser
echo "PREPARING HDFS FOR HDUSER"
$HADOOP_HOME/bin/hdfs dfs -mkdir -p /user/hduser
$HADOOP_HOME/bin/hdfs dfs -chown hduser /user/hduser

# Preparing Tez
echo "PREPARING HDFS FOR TEZ"
$HADOOP_HOME/bin/hdfs dfs -mkdir -p /apps/tez
$HADOOP_HOME/bin/hdfs dfs -put /root/Install/apache-tez-0.9.2-dist/tez-dist/target/tez-0.9.2.tar.gz /apps/tez/

echo "STOPING ALL HADOOP SERVICES"
$HADOOP_HOME/sbin/stop-all.sh

echo "STARTING MYSQL"
/etc/init.d/mysql start

echo "CREATING METASTORE SCHEMA FOR MYSQL"
$HIVE_HOME/bin/schematool -dbType mysql -initSchema

echo "Removing Install dir"
rm -rf /root/Install

echo "Setup is done! Now, runt start-hadoop.sh"

exit 0
