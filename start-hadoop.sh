# start ssh server
/etc/init.d/ssh start
 
# format namenode
#yes | $HADOOP_HOME/bin/hdfs namenode -format
 
# start hadoop
echo "Starting dfs, yearn and map-reduce services"
$HADOOP_HOME/sbin/start-dfs.sh
$HADOOP_HOME/sbin/start-yarn.sh
$HADOOP_HOME/sbin/mr-jobhistory-daemon.sh start historyserver

# start mysql service
echo "Starting MySQL service"
/etc/init.d/mysql start

# start hive and metastore
echo "Starting Hive in background"
nohup $HIVE_HOME/bin/hiveserver2 >/var/log/nohup-hiveserver2.log 2>/var/log/nohup-hiveserver2.err &
nohup $HIVE_HOME/bin/hive --service metastore >/var/log/nohup-metastore.log 2>/var/log/nohup-metastore.err &

# echo "Waiting for hive services"
sleep 10

echo "Setting permissions on /tmp"

chmod -R 777 /tmp

echo "All done!"

# keep container running
tail -f /dev/null
